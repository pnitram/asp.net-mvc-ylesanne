﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Web;
using System.Xml;
using System.Xml.Serialization;

namespace AmazonSearch
{
    internal static class Helpers
    {

        public static T ParseXml<T>(this string @this) where T : class
        {
            byte[] byteArray = Encoding.UTF8.GetBytes(@this);

            MemoryStream stream = new MemoryStream(byteArray);

            var reader = XmlReader.Create(stream, new XmlReaderSettings() { ConformanceLevel = ConformanceLevel.Document });
            return new XmlSerializer(typeof(T)).Deserialize(reader) as T;
        }
    }
}