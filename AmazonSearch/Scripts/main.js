﻿$(function () {
    $(".dropdown-menu > li > a").on("click", function () {
        var currency = $(this).html();

        changePrices($("#currentCurrency").html(), currency);
        setNewCurrency(currency)
    });

    if ($("#currentCurrency").html() != getCookie("currency")) {
        changePrices($("#currentCurrency").html(), getCookie("currency"));
    }
});

function setNewCurrency(newCurrency) {
    $("#currentCurrency").html(newCurrency)
    setCookie("currency", newCurrency);
}

function getRate(currency, callback) {
    $.ajax({
        url: "/Rates",
        success: function (e) {
            var rate = e.rates[currency];
            callback(rate);
        }
    });
}

function changePrices(prevCurrency, newCurrency) {
    getRate(prevCurrency, function (prevRate) {
        getRate(newCurrency, function (newRate) {
            if(prevRate == undefined || newRate == undefined){
                return;
            }
            $("#productsContainer > .list-group > li").each(function (index, elem) {
                var oldPrice = $(elem).find("#price").html();
                var newPrice = oldPrice / prevRate * newRate;

                if(newCurrency.trim().length == 0){
                    newCurrency = "USD";
                }

                $(elem).find("#price").html(newPrice.toFixed(2));
                $(elem).find("#currency").html(newCurrency);
            });
        });
    });
}

function setCookie(cname, cvalue, exdays) {
    var d = new Date();
    d.setTime(d.getTime() + (exdays * 24 * 60 * 60 * 1000));
    var expires = "expires=" + d.toUTCString();
    document.cookie = cname + "=" + cvalue + "; " + expires;
}

function getCookie(cname) {
    var name = cname + "=";
    var ca = document.cookie.split(';');
    for (var i = 0; i < ca.length; i++) {
        var c = ca[i];
        while (c.charAt(0) == ' ') {
            c = c.substring(1);
        }
        if (c.indexOf(name) == 0) {
            return c.substring(name.length, c.length);
        }
    }
    return "";
}