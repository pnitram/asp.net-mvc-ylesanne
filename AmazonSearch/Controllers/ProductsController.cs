﻿using AmazonSearch.Models;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;

namespace AmazonSearch.Controllers
{
    public class ProductsController : Controller
    {
        private const string ACCESS_KEY_ID = "";
        private const string SECRET_KEY = "";
        private const string ASSOCIATE_ID = "";

        Response response;

        // GET: Products
        [HttpGet]
        public ActionResult Index(string keyword, string page, string currency)
        {
            response = new Response() { keyword = keyword };

            int pageN;
            bool isNumeric = int.TryParse(page, out pageN);

            if (!isNumeric)
            {
                pageN = 1;
            }

            if (currency == null || !isValidPropertyOfRates(currency))
            {
                currency = "USD";
            }

            var currencyCookie = Request.Cookies["currency"];

            if (currencyCookie != null && isValidPropertyOfRates(currencyCookie.Value))
            {
                currency = Request.Cookies["currency"].Value;
            }

            var products = getItems(keyword, pageN, currency);

            response.product_count = products.Count;
            response.products = products;
            response.page = pageN;

            response.currency = currency;

            response.error = products.Count == 0;

            //return Json(response, JsonRequestBehavior.AllowGet);
            return View(response);
        }

        private List<Product> getItems(string keyword, int page, string currency)
        {
            List<Product> products = new List<Product>();

            SignedRequestHelper helper = new SignedRequestHelper(ACCESS_KEY_ID, SECRET_KEY, "webservices.amazon.com");

            IDictionary<string, string> r1 = new Dictionary<string, string>();
            r1["Service"] = "AWSECommerceService";
            r1["AssociateTag"] = ASSOCIATE_ID;
            r1["Operation"] = "ItemSearch";
            r1["Keywords"] = keyword;
            r1["SearchIndex"] = "All";
            r1["ItemPage"] = page.ToString();
            r1["ResponseGroup"] = "Medium";

            if (String.IsNullOrEmpty(keyword))
            {
                return products;
            }

            using (WebClient client = new WebClient())
            {
                string s = client.DownloadString(helper.Sign(r1));

                var result = s.ParseXml<ItemSearchResponse>();

                foreach (var i in result.Items.Item)
                {
                    string price = "NO PRICE";

                    try
                    {
                        price = i.OfferSummary.LowestNewPrice.FormattedPrice;
                    }
                    catch (Exception e)
                    {
                        Console.WriteLine(e);
                        try
                        {
                            price = i.ItemAttributes.ListPrice.FormattedPrice;
                        }
                        catch (Exception e2)
                        {
                            Console.WriteLine(e2);
                        }
                    }

                    products.Add(new Product
                    {
                        title = i.ItemAttributes.Title,
                        price = price
                    });
                }
            }

            return applyCurrency(products, currency);
        }

        private List<Product> applyCurrency(List<Product> products, string currency)
        {
            using (StreamReader r = new StreamReader(Server.MapPath("~/Models/rates.json")))
            {
                string json = r.ReadToEnd();
                ExchangeRatesObject result = JsonConvert.DeserializeObject<ExchangeRatesObject>(json);

                foreach (Product p in products)
                {
                    float price;
                    bool isNumeric = float.TryParse(p.price.Substring(1), out price);

                    if (isNumeric)
                    {
                        float difference = 1;
                        difference = float.Parse(result.rates.GetType().GetProperty(currency.ToUpper()).GetValue(result.rates, null).ToString());
                        price *= difference;
                        p.price = Math.Round(price, 2).ToString("0.00");
                    }
                }
            }


            return products;
        }

        private bool isValidPropertyOfRates(string propertyName)
        {
            return new Rates().GetType().GetProperty(propertyName) != null;
        }
    }

    public class Response
    {
        public bool error { get; set; }
        public string keyword { get; set; }
        public int page { get; set; }
        public string currency { get; set; }
        public int product_count { get; set; }
        public List<Product> products { get; set; }
    }
}