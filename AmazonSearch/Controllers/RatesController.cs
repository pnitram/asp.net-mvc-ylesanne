﻿using AmazonSearch.Models;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace AmazonSearch.Controllers
{
    public class RatesController : Controller
    {
        // GET: Rates
        [HttpGet]
        public ActionResult Index()
        {
            ExchangeRatesObject result = new ExchangeRatesObject();
            using (StreamReader r = new StreamReader(Server.MapPath("~/Models/rates.json")))
            {
                string json = r.ReadToEnd();
                result = JsonConvert.DeserializeObject<ExchangeRatesObject>(json);
            }
            return Json(result, JsonRequestBehavior.AllowGet);
        }
    }
}