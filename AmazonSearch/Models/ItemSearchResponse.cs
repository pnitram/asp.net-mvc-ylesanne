﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Xml.Serialization;

namespace AmazonSearch
{
    [XmlRoot(ElementName = "Argument", Namespace = "http://webservices.amazon.com/AWSECommerceService/2011-08-01")]
    public class Argument
    {
        [XmlAttribute(AttributeName = "Name")]
        public string Name { get; set; }
        [XmlAttribute(AttributeName = "Value")]
        public string Value { get; set; }
    }

    [XmlRoot(ElementName = "Arguments", Namespace = "http://webservices.amazon.com/AWSECommerceService/2011-08-01")]
    public class Arguments
    {
        [XmlElement(ElementName = "Argument", Namespace = "http://webservices.amazon.com/AWSECommerceService/2011-08-01")]
        public List<Argument> Argument { get; set; }
    }

    [XmlRoot(ElementName = "OperationRequest", Namespace = "http://webservices.amazon.com/AWSECommerceService/2011-08-01")]
    public class OperationRequest
    {
        [XmlElement(ElementName = "RequestId", Namespace = "http://webservices.amazon.com/AWSECommerceService/2011-08-01")]
        public string RequestId { get; set; }
        [XmlElement(ElementName = "Arguments", Namespace = "http://webservices.amazon.com/AWSECommerceService/2011-08-01")]
        public Arguments Arguments { get; set; }
        [XmlElement(ElementName = "RequestProcessingTime", Namespace = "http://webservices.amazon.com/AWSECommerceService/2011-08-01")]
        public string RequestProcessingTime { get; set; }
    }

    [XmlRoot(ElementName = "ItemSearchRequest", Namespace = "http://webservices.amazon.com/AWSECommerceService/2011-08-01")]
    public class ItemSearchRequest
    {
        [XmlElement(ElementName = "ItemPage", Namespace = "http://webservices.amazon.com/AWSECommerceService/2011-08-01")]
        public string ItemPage { get; set; }
        [XmlElement(ElementName = "Keywords", Namespace = "http://webservices.amazon.com/AWSECommerceService/2011-08-01")]
        public string Keywords { get; set; }
        [XmlElement(ElementName = "ResponseGroup", Namespace = "http://webservices.amazon.com/AWSECommerceService/2011-08-01")]
        public string ResponseGroup { get; set; }
        [XmlElement(ElementName = "SearchIndex", Namespace = "http://webservices.amazon.com/AWSECommerceService/2011-08-01")]
        public string SearchIndex { get; set; }
    }

    [XmlRoot(ElementName = "Request", Namespace = "http://webservices.amazon.com/AWSECommerceService/2011-08-01")]
    public class Request
    {
        [XmlElement(ElementName = "IsValid", Namespace = "http://webservices.amazon.com/AWSECommerceService/2011-08-01")]
        public string IsValid { get; set; }
        [XmlElement(ElementName = "ItemSearchRequest", Namespace = "http://webservices.amazon.com/AWSECommerceService/2011-08-01")]
        public ItemSearchRequest ItemSearchRequest { get; set; }
    }

    [XmlRoot(ElementName = "ItemLink", Namespace = "http://webservices.amazon.com/AWSECommerceService/2011-08-01")]
    public class ItemLink
    {
        [XmlElement(ElementName = "Description", Namespace = "http://webservices.amazon.com/AWSECommerceService/2011-08-01")]
        public string Description { get; set; }
        [XmlElement(ElementName = "URL", Namespace = "http://webservices.amazon.com/AWSECommerceService/2011-08-01")]
        public string URL { get; set; }
    }

    [XmlRoot(ElementName = "ItemLinks", Namespace = "http://webservices.amazon.com/AWSECommerceService/2011-08-01")]
    public class ItemLinks
    {
        [XmlElement(ElementName = "ItemLink", Namespace = "http://webservices.amazon.com/AWSECommerceService/2011-08-01")]
        public List<ItemLink> ItemLink { get; set; }
    }

    [XmlRoot(ElementName = "Height", Namespace = "http://webservices.amazon.com/AWSECommerceService/2011-08-01")]
    public class Height
    {
        [XmlAttribute(AttributeName = "Units")]
        public string Units { get; set; }
        [XmlText]
        public string Text { get; set; }
    }

    [XmlRoot(ElementName = "Width", Namespace = "http://webservices.amazon.com/AWSECommerceService/2011-08-01")]
    public class Width
    {
        [XmlAttribute(AttributeName = "Units")]
        public string Units { get; set; }
        [XmlText]
        public string Text { get; set; }
    }

    [XmlRoot(ElementName = "SmallImage", Namespace = "http://webservices.amazon.com/AWSECommerceService/2011-08-01")]
    public class SmallImage
    {
        [XmlElement(ElementName = "URL", Namespace = "http://webservices.amazon.com/AWSECommerceService/2011-08-01")]
        public string URL { get; set; }
        [XmlElement(ElementName = "Height", Namespace = "http://webservices.amazon.com/AWSECommerceService/2011-08-01")]
        public Height Height { get; set; }
        [XmlElement(ElementName = "Width", Namespace = "http://webservices.amazon.com/AWSECommerceService/2011-08-01")]
        public Width Width { get; set; }
    }

    [XmlRoot(ElementName = "MediumImage", Namespace = "http://webservices.amazon.com/AWSECommerceService/2011-08-01")]
    public class MediumImage
    {
        [XmlElement(ElementName = "URL", Namespace = "http://webservices.amazon.com/AWSECommerceService/2011-08-01")]
        public string URL { get; set; }
        [XmlElement(ElementName = "Height", Namespace = "http://webservices.amazon.com/AWSECommerceService/2011-08-01")]
        public Height Height { get; set; }
        [XmlElement(ElementName = "Width", Namespace = "http://webservices.amazon.com/AWSECommerceService/2011-08-01")]
        public Width Width { get; set; }
    }

    [XmlRoot(ElementName = "LargeImage", Namespace = "http://webservices.amazon.com/AWSECommerceService/2011-08-01")]
    public class LargeImage
    {
        [XmlElement(ElementName = "URL", Namespace = "http://webservices.amazon.com/AWSECommerceService/2011-08-01")]
        public string URL { get; set; }
        [XmlElement(ElementName = "Height", Namespace = "http://webservices.amazon.com/AWSECommerceService/2011-08-01")]
        public Height Height { get; set; }
        [XmlElement(ElementName = "Width", Namespace = "http://webservices.amazon.com/AWSECommerceService/2011-08-01")]
        public Width Width { get; set; }
    }

    [XmlRoot(ElementName = "SwatchImage", Namespace = "http://webservices.amazon.com/AWSECommerceService/2011-08-01")]
    public class SwatchImage
    {
        [XmlElement(ElementName = "URL", Namespace = "http://webservices.amazon.com/AWSECommerceService/2011-08-01")]
        public string URL { get; set; }
        [XmlElement(ElementName = "Height", Namespace = "http://webservices.amazon.com/AWSECommerceService/2011-08-01")]
        public Height Height { get; set; }
        [XmlElement(ElementName = "Width", Namespace = "http://webservices.amazon.com/AWSECommerceService/2011-08-01")]
        public Width Width { get; set; }
    }

    [XmlRoot(ElementName = "ThumbnailImage", Namespace = "http://webservices.amazon.com/AWSECommerceService/2011-08-01")]
    public class ThumbnailImage
    {
        [XmlElement(ElementName = "URL", Namespace = "http://webservices.amazon.com/AWSECommerceService/2011-08-01")]
        public string URL { get; set; }
        [XmlElement(ElementName = "Height", Namespace = "http://webservices.amazon.com/AWSECommerceService/2011-08-01")]
        public Height Height { get; set; }
        [XmlElement(ElementName = "Width", Namespace = "http://webservices.amazon.com/AWSECommerceService/2011-08-01")]
        public Width Width { get; set; }
    }

    [XmlRoot(ElementName = "TinyImage", Namespace = "http://webservices.amazon.com/AWSECommerceService/2011-08-01")]
    public class TinyImage
    {
        [XmlElement(ElementName = "URL", Namespace = "http://webservices.amazon.com/AWSECommerceService/2011-08-01")]
        public string URL { get; set; }
        [XmlElement(ElementName = "Height", Namespace = "http://webservices.amazon.com/AWSECommerceService/2011-08-01")]
        public Height Height { get; set; }
        [XmlElement(ElementName = "Width", Namespace = "http://webservices.amazon.com/AWSECommerceService/2011-08-01")]
        public Width Width { get; set; }
    }

    [XmlRoot(ElementName = "ImageSet", Namespace = "http://webservices.amazon.com/AWSECommerceService/2011-08-01")]
    public class ImageSet
    {
        [XmlElement(ElementName = "SwatchImage", Namespace = "http://webservices.amazon.com/AWSECommerceService/2011-08-01")]
        public SwatchImage SwatchImage { get; set; }
        [XmlElement(ElementName = "SmallImage", Namespace = "http://webservices.amazon.com/AWSECommerceService/2011-08-01")]
        public SmallImage SmallImage { get; set; }
        [XmlElement(ElementName = "ThumbnailImage", Namespace = "http://webservices.amazon.com/AWSECommerceService/2011-08-01")]
        public ThumbnailImage ThumbnailImage { get; set; }
        [XmlElement(ElementName = "TinyImage", Namespace = "http://webservices.amazon.com/AWSECommerceService/2011-08-01")]
        public TinyImage TinyImage { get; set; }
        [XmlElement(ElementName = "MediumImage", Namespace = "http://webservices.amazon.com/AWSECommerceService/2011-08-01")]
        public MediumImage MediumImage { get; set; }
        [XmlElement(ElementName = "LargeImage", Namespace = "http://webservices.amazon.com/AWSECommerceService/2011-08-01")]
        public LargeImage LargeImage { get; set; }
        [XmlAttribute(AttributeName = "Category")]
        public string Category { get; set; }
    }

    [XmlRoot(ElementName = "ImageSets", Namespace = "http://webservices.amazon.com/AWSECommerceService/2011-08-01")]
    public class ImageSets
    {
        [XmlElement(ElementName = "ImageSet", Namespace = "http://webservices.amazon.com/AWSECommerceService/2011-08-01")]
        public List<ImageSet> ImageSet { get; set; }
    }

    [XmlRoot(ElementName = "EANList", Namespace = "http://webservices.amazon.com/AWSECommerceService/2011-08-01")]
    public class EANList
    {
        [XmlElement(ElementName = "EANListElement", Namespace = "http://webservices.amazon.com/AWSECommerceService/2011-08-01")]
        public List<string> EANListElement { get; set; }
    }

    [XmlRoot(ElementName = "Length", Namespace = "http://webservices.amazon.com/AWSECommerceService/2011-08-01")]
    public class Length
    {
        [XmlAttribute(AttributeName = "Units")]
        public string Units { get; set; }
        [XmlText]
        public string Text { get; set; }
    }

    [XmlRoot(ElementName = "Weight", Namespace = "http://webservices.amazon.com/AWSECommerceService/2011-08-01")]
    public class Weight
    {
        [XmlAttribute(AttributeName = "Units")]
        public string Units { get; set; }
        [XmlText]
        public string Text { get; set; }
    }

    [XmlRoot(ElementName = "ItemDimensions", Namespace = "http://webservices.amazon.com/AWSECommerceService/2011-08-01")]
    public class ItemDimensions
    {
        [XmlElement(ElementName = "Height", Namespace = "http://webservices.amazon.com/AWSECommerceService/2011-08-01")]
        public Height Height { get; set; }
        [XmlElement(ElementName = "Length", Namespace = "http://webservices.amazon.com/AWSECommerceService/2011-08-01")]
        public Length Length { get; set; }
        [XmlElement(ElementName = "Weight", Namespace = "http://webservices.amazon.com/AWSECommerceService/2011-08-01")]
        public Weight Weight { get; set; }
        [XmlElement(ElementName = "Width", Namespace = "http://webservices.amazon.com/AWSECommerceService/2011-08-01")]
        public Width Width { get; set; }
    }

    [XmlRoot(ElementName = "ListPrice", Namespace = "http://webservices.amazon.com/AWSECommerceService/2011-08-01")]
    public class ListPrice
    {
        [XmlElement(ElementName = "Amount", Namespace = "http://webservices.amazon.com/AWSECommerceService/2011-08-01")]
        public string Amount { get; set; }
        [XmlElement(ElementName = "CurrencyCode", Namespace = "http://webservices.amazon.com/AWSECommerceService/2011-08-01")]
        public string CurrencyCode { get; set; }
        [XmlElement(ElementName = "FormattedPrice", Namespace = "http://webservices.amazon.com/AWSECommerceService/2011-08-01")]
        public string FormattedPrice { get; set; }
    }

    [XmlRoot(ElementName = "PackageDimensions", Namespace = "http://webservices.amazon.com/AWSECommerceService/2011-08-01")]
    public class PackageDimensions
    {
        [XmlElement(ElementName = "Height", Namespace = "http://webservices.amazon.com/AWSECommerceService/2011-08-01")]
        public Height Height { get; set; }
        [XmlElement(ElementName = "Length", Namespace = "http://webservices.amazon.com/AWSECommerceService/2011-08-01")]
        public Length Length { get; set; }
        [XmlElement(ElementName = "Weight", Namespace = "http://webservices.amazon.com/AWSECommerceService/2011-08-01")]
        public Weight Weight { get; set; }
        [XmlElement(ElementName = "Width", Namespace = "http://webservices.amazon.com/AWSECommerceService/2011-08-01")]
        public Width Width { get; set; }
    }

    [XmlRoot(ElementName = "UPCList", Namespace = "http://webservices.amazon.com/AWSECommerceService/2011-08-01")]
    public class UPCList
    {
        [XmlElement(ElementName = "UPCListElement", Namespace = "http://webservices.amazon.com/AWSECommerceService/2011-08-01")]
        public List<string> UPCListElement { get; set; }
    }

    [XmlRoot(ElementName = "ItemAttributes", Namespace = "http://webservices.amazon.com/AWSECommerceService/2011-08-01")]
    public class ItemAttributes
    {
        [XmlElement(ElementName = "Binding", Namespace = "http://webservices.amazon.com/AWSECommerceService/2011-08-01")]
        public string Binding { get; set; }
        [XmlElement(ElementName = "Brand", Namespace = "http://webservices.amazon.com/AWSECommerceService/2011-08-01")]
        public string Brand { get; set; }
        [XmlElement(ElementName = "EAN", Namespace = "http://webservices.amazon.com/AWSECommerceService/2011-08-01")]
        public string EAN { get; set; }
        [XmlElement(ElementName = "EANList", Namespace = "http://webservices.amazon.com/AWSECommerceService/2011-08-01")]
        public EANList EANList { get; set; }
        [XmlElement(ElementName = "Feature", Namespace = "http://webservices.amazon.com/AWSECommerceService/2011-08-01")]
        public List<string> Feature { get; set; }
        [XmlElement(ElementName = "ItemDimensions", Namespace = "http://webservices.amazon.com/AWSECommerceService/2011-08-01")]
        public ItemDimensions ItemDimensions { get; set; }
        [XmlElement(ElementName = "Label", Namespace = "http://webservices.amazon.com/AWSECommerceService/2011-08-01")]
        public string Label { get; set; }
        [XmlElement(ElementName = "ListPrice", Namespace = "http://webservices.amazon.com/AWSECommerceService/2011-08-01")]
        public ListPrice ListPrice { get; set; }
        [XmlElement(ElementName = "Manufacturer", Namespace = "http://webservices.amazon.com/AWSECommerceService/2011-08-01")]
        public string Manufacturer { get; set; }
        [XmlElement(ElementName = "Model", Namespace = "http://webservices.amazon.com/AWSECommerceService/2011-08-01")]
        public string Model { get; set; }
        [XmlElement(ElementName = "MPN", Namespace = "http://webservices.amazon.com/AWSECommerceService/2011-08-01")]
        public string MPN { get; set; }
        [XmlElement(ElementName = "PackageDimensions", Namespace = "http://webservices.amazon.com/AWSECommerceService/2011-08-01")]
        public PackageDimensions PackageDimensions { get; set; }
        [XmlElement(ElementName = "PackageQuantity", Namespace = "http://webservices.amazon.com/AWSECommerceService/2011-08-01")]
        public string PackageQuantity { get; set; }
        [XmlElement(ElementName = "PartNumber", Namespace = "http://webservices.amazon.com/AWSECommerceService/2011-08-01")]
        public string PartNumber { get; set; }
        [XmlElement(ElementName = "ProductGroup", Namespace = "http://webservices.amazon.com/AWSECommerceService/2011-08-01")]
        public string ProductGroup { get; set; }
        [XmlElement(ElementName = "ProductTypeName", Namespace = "http://webservices.amazon.com/AWSECommerceService/2011-08-01")]
        public string ProductTypeName { get; set; }
        [XmlElement(ElementName = "Publisher", Namespace = "http://webservices.amazon.com/AWSECommerceService/2011-08-01")]
        public string Publisher { get; set; }
        [XmlElement(ElementName = "Studio", Namespace = "http://webservices.amazon.com/AWSECommerceService/2011-08-01")]
        public string Studio { get; set; }
        [XmlElement(ElementName = "Title", Namespace = "http://webservices.amazon.com/AWSECommerceService/2011-08-01")]
        public string Title { get; set; }
        [XmlElement(ElementName = "UPC", Namespace = "http://webservices.amazon.com/AWSECommerceService/2011-08-01")]
        public string UPC { get; set; }
        [XmlElement(ElementName = "UPCList", Namespace = "http://webservices.amazon.com/AWSECommerceService/2011-08-01")]
        public UPCList UPCList { get; set; }
        [XmlElement(ElementName = "LegalDisclaimer", Namespace = "http://webservices.amazon.com/AWSECommerceService/2011-08-01")]
        public string LegalDisclaimer { get; set; }
        [XmlElement(ElementName = "CatalogNumberList", Namespace = "http://webservices.amazon.com/AWSECommerceService/2011-08-01")]
        public CatalogNumberList CatalogNumberList { get; set; }
        [XmlElement(ElementName = "Color", Namespace = "http://webservices.amazon.com/AWSECommerceService/2011-08-01")]
        public string Color { get; set; }
        [XmlElement(ElementName = "IsAutographed", Namespace = "http://webservices.amazon.com/AWSECommerceService/2011-08-01")]
        public string IsAutographed { get; set; }
        [XmlElement(ElementName = "IsMemorabilia", Namespace = "http://webservices.amazon.com/AWSECommerceService/2011-08-01")]
        public string IsMemorabilia { get; set; }
        [XmlElement(ElementName = "IsAdultProduct", Namespace = "http://webservices.amazon.com/AWSECommerceService/2011-08-01")]
        public string IsAdultProduct { get; set; }
        [XmlElement(ElementName = "Languages", Namespace = "http://webservices.amazon.com/AWSECommerceService/2011-08-01")]
        public Languages Languages { get; set; }
        [XmlElement(ElementName = "NumberOfItems", Namespace = "http://webservices.amazon.com/AWSECommerceService/2011-08-01")]
        public string NumberOfItems { get; set; }
        [XmlElement(ElementName = "Warranty", Namespace = "http://webservices.amazon.com/AWSECommerceService/2011-08-01")]
        public string Warranty { get; set; }
        [XmlElement(ElementName = "Edition", Namespace = "http://webservices.amazon.com/AWSECommerceService/2011-08-01")]
        public string Edition { get; set; }
        [XmlElement(ElementName = "ESRBAgeRating", Namespace = "http://webservices.amazon.com/AWSECommerceService/2011-08-01")]
        public string ESRBAgeRating { get; set; }
        [XmlElement(ElementName = "HardwarePlatform", Namespace = "http://webservices.amazon.com/AWSECommerceService/2011-08-01")]
        public string HardwarePlatform { get; set; }
        [XmlElement(ElementName = "IsEligibleForTradeIn", Namespace = "http://webservices.amazon.com/AWSECommerceService/2011-08-01")]
        public string IsEligibleForTradeIn { get; set; }
        [XmlElement(ElementName = "OperatingSystem", Namespace = "http://webservices.amazon.com/AWSECommerceService/2011-08-01")]
        public string OperatingSystem { get; set; }
        [XmlElement(ElementName = "Platform", Namespace = "http://webservices.amazon.com/AWSECommerceService/2011-08-01")]
        public string Platform { get; set; }
        [XmlElement(ElementName = "ReleaseDate", Namespace = "http://webservices.amazon.com/AWSECommerceService/2011-08-01")]
        public string ReleaseDate { get; set; }
        [XmlElement(ElementName = "TradeInValue", Namespace = "http://webservices.amazon.com/AWSECommerceService/2011-08-01")]
        public TradeInValue TradeInValue { get; set; }
    }

    [XmlRoot(ElementName = "LowestNewPrice", Namespace = "http://webservices.amazon.com/AWSECommerceService/2011-08-01")]
    public class LowestNewPrice
    {
        [XmlElement(ElementName = "Amount", Namespace = "http://webservices.amazon.com/AWSECommerceService/2011-08-01")]
        public string Amount { get; set; }
        [XmlElement(ElementName = "CurrencyCode", Namespace = "http://webservices.amazon.com/AWSECommerceService/2011-08-01")]
        public string CurrencyCode { get; set; }
        [XmlElement(ElementName = "FormattedPrice", Namespace = "http://webservices.amazon.com/AWSECommerceService/2011-08-01")]
        public string FormattedPrice { get; set; }
    }

    [XmlRoot(ElementName = "OfferSummary", Namespace = "http://webservices.amazon.com/AWSECommerceService/2011-08-01")]
    public class OfferSummary
    {
        [XmlElement(ElementName = "LowestNewPrice", Namespace = "http://webservices.amazon.com/AWSECommerceService/2011-08-01")]
        public LowestNewPrice LowestNewPrice { get; set; }
        [XmlElement(ElementName = "TotalNew", Namespace = "http://webservices.amazon.com/AWSECommerceService/2011-08-01")]
        public string TotalNew { get; set; }
        [XmlElement(ElementName = "TotalUsed", Namespace = "http://webservices.amazon.com/AWSECommerceService/2011-08-01")]
        public string TotalUsed { get; set; }
        [XmlElement(ElementName = "TotalCollectible", Namespace = "http://webservices.amazon.com/AWSECommerceService/2011-08-01")]
        public string TotalCollectible { get; set; }
        [XmlElement(ElementName = "TotalRefurbished", Namespace = "http://webservices.amazon.com/AWSECommerceService/2011-08-01")]
        public string TotalRefurbished { get; set; }
        [XmlElement(ElementName = "LowestUsedPrice", Namespace = "http://webservices.amazon.com/AWSECommerceService/2011-08-01")]
        public LowestUsedPrice LowestUsedPrice { get; set; }
        [XmlElement(ElementName = "LowestRefurbishedPrice", Namespace = "http://webservices.amazon.com/AWSECommerceService/2011-08-01")]
        public LowestRefurbishedPrice LowestRefurbishedPrice { get; set; }
    }

    [XmlRoot(ElementName = "EditorialReview", Namespace = "http://webservices.amazon.com/AWSECommerceService/2011-08-01")]
    public class EditorialReview
    {
        [XmlElement(ElementName = "Source", Namespace = "http://webservices.amazon.com/AWSECommerceService/2011-08-01")]
        public string Source { get; set; }
        [XmlElement(ElementName = "Content", Namespace = "http://webservices.amazon.com/AWSECommerceService/2011-08-01")]
        public string Content { get; set; }
        [XmlElement(ElementName = "IsLinkSuppressed", Namespace = "http://webservices.amazon.com/AWSECommerceService/2011-08-01")]
        public string IsLinkSuppressed { get; set; }
    }

    [XmlRoot(ElementName = "EditorialReviews", Namespace = "http://webservices.amazon.com/AWSECommerceService/2011-08-01")]
    public class EditorialReviews
    {
        [XmlElement(ElementName = "EditorialReview", Namespace = "http://webservices.amazon.com/AWSECommerceService/2011-08-01")]
        public EditorialReview EditorialReview { get; set; }
    }

    [XmlRoot(ElementName = "Item", Namespace = "http://webservices.amazon.com/AWSECommerceService/2011-08-01")]
    public class Item
    {
        [XmlElement(ElementName = "ASIN", Namespace = "http://webservices.amazon.com/AWSECommerceService/2011-08-01")]
        public string ASIN { get; set; }
        [XmlElement(ElementName = "DetailPageURL", Namespace = "http://webservices.amazon.com/AWSECommerceService/2011-08-01")]
        public string DetailPageURL { get; set; }
        [XmlElement(ElementName = "ItemLinks", Namespace = "http://webservices.amazon.com/AWSECommerceService/2011-08-01")]
        public ItemLinks ItemLinks { get; set; }
        [XmlElement(ElementName = "SmallImage", Namespace = "http://webservices.amazon.com/AWSECommerceService/2011-08-01")]
        public SmallImage SmallImage { get; set; }
        [XmlElement(ElementName = "MediumImage", Namespace = "http://webservices.amazon.com/AWSECommerceService/2011-08-01")]
        public MediumImage MediumImage { get; set; }
        [XmlElement(ElementName = "LargeImage", Namespace = "http://webservices.amazon.com/AWSECommerceService/2011-08-01")]
        public LargeImage LargeImage { get; set; }
        [XmlElement(ElementName = "ImageSets", Namespace = "http://webservices.amazon.com/AWSECommerceService/2011-08-01")]
        public ImageSets ImageSets { get; set; }
        [XmlElement(ElementName = "ItemAttributes", Namespace = "http://webservices.amazon.com/AWSECommerceService/2011-08-01")]
        public ItemAttributes ItemAttributes { get; set; }
        [XmlElement(ElementName = "OfferSummary", Namespace = "http://webservices.amazon.com/AWSECommerceService/2011-08-01")]
        public OfferSummary OfferSummary { get; set; }
        [XmlElement(ElementName = "EditorialReviews", Namespace = "http://webservices.amazon.com/AWSECommerceService/2011-08-01")]
        public EditorialReviews EditorialReviews { get; set; }
        [XmlElement(ElementName = "ParentASIN", Namespace = "http://webservices.amazon.com/AWSECommerceService/2011-08-01")]
        public string ParentASIN { get; set; }
        [XmlElement(ElementName = "SalesRank", Namespace = "http://webservices.amazon.com/AWSECommerceService/2011-08-01")]
        public string SalesRank { get; set; }
    }

    [XmlRoot(ElementName = "LowestUsedPrice", Namespace = "http://webservices.amazon.com/AWSECommerceService/2011-08-01")]
    public class LowestUsedPrice
    {
        [XmlElement(ElementName = "Amount", Namespace = "http://webservices.amazon.com/AWSECommerceService/2011-08-01")]
        public string Amount { get; set; }
        [XmlElement(ElementName = "CurrencyCode", Namespace = "http://webservices.amazon.com/AWSECommerceService/2011-08-01")]
        public string CurrencyCode { get; set; }
        [XmlElement(ElementName = "FormattedPrice", Namespace = "http://webservices.amazon.com/AWSECommerceService/2011-08-01")]
        public string FormattedPrice { get; set; }
    }

    [XmlRoot(ElementName = "CatalogNumberList", Namespace = "http://webservices.amazon.com/AWSECommerceService/2011-08-01")]
    public class CatalogNumberList
    {
        [XmlElement(ElementName = "CatalogNumberListElement", Namespace = "http://webservices.amazon.com/AWSECommerceService/2011-08-01")]
        public List<string> CatalogNumberListElement { get; set; }
    }

    [XmlRoot(ElementName = "Language", Namespace = "http://webservices.amazon.com/AWSECommerceService/2011-08-01")]
    public class Language
    {
        [XmlElement(ElementName = "Name", Namespace = "http://webservices.amazon.com/AWSECommerceService/2011-08-01")]
        public string Name { get; set; }
        [XmlElement(ElementName = "Type", Namespace = "http://webservices.amazon.com/AWSECommerceService/2011-08-01")]
        public string Type { get; set; }
    }

    [XmlRoot(ElementName = "Languages", Namespace = "http://webservices.amazon.com/AWSECommerceService/2011-08-01")]
    public class Languages
    {
        [XmlElement(ElementName = "Language", Namespace = "http://webservices.amazon.com/AWSECommerceService/2011-08-01")]
        public Language Language { get; set; }
    }

    [XmlRoot(ElementName = "LowestRefurbishedPrice", Namespace = "http://webservices.amazon.com/AWSECommerceService/2011-08-01")]
    public class LowestRefurbishedPrice
    {
        [XmlElement(ElementName = "Amount", Namespace = "http://webservices.amazon.com/AWSECommerceService/2011-08-01")]
        public string Amount { get; set; }
        [XmlElement(ElementName = "CurrencyCode", Namespace = "http://webservices.amazon.com/AWSECommerceService/2011-08-01")]
        public string CurrencyCode { get; set; }
        [XmlElement(ElementName = "FormattedPrice", Namespace = "http://webservices.amazon.com/AWSECommerceService/2011-08-01")]
        public string FormattedPrice { get; set; }
    }

    [XmlRoot(ElementName = "TradeInValue", Namespace = "http://webservices.amazon.com/AWSECommerceService/2011-08-01")]
    public class TradeInValue
    {
        [XmlElement(ElementName = "Amount", Namespace = "http://webservices.amazon.com/AWSECommerceService/2011-08-01")]
        public string Amount { get; set; }
        [XmlElement(ElementName = "CurrencyCode", Namespace = "http://webservices.amazon.com/AWSECommerceService/2011-08-01")]
        public string CurrencyCode { get; set; }
        [XmlElement(ElementName = "FormattedPrice", Namespace = "http://webservices.amazon.com/AWSECommerceService/2011-08-01")]
        public string FormattedPrice { get; set; }
    }

    [XmlRoot(ElementName = "Items", Namespace = "http://webservices.amazon.com/AWSECommerceService/2011-08-01")]
    public class Items
    {
        [XmlElement(ElementName = "Request", Namespace = "http://webservices.amazon.com/AWSECommerceService/2011-08-01")]
        public Request Request { get; set; }
        [XmlElement(ElementName = "TotalResults", Namespace = "http://webservices.amazon.com/AWSECommerceService/2011-08-01")]
        public string TotalResults { get; set; }
        [XmlElement(ElementName = "TotalPages", Namespace = "http://webservices.amazon.com/AWSECommerceService/2011-08-01")]
        public string TotalPages { get; set; }
        [XmlElement(ElementName = "MoreSearchResultsUrl", Namespace = "http://webservices.amazon.com/AWSECommerceService/2011-08-01")]
        public string MoreSearchResultsUrl { get; set; }
        [XmlElement(ElementName = "Item", Namespace = "http://webservices.amazon.com/AWSECommerceService/2011-08-01")]
        public List<Item> Item { get; set; }
    }

    [XmlRoot(ElementName = "ItemSearchResponse", Namespace = "http://webservices.amazon.com/AWSECommerceService/2011-08-01")]
    public class ItemSearchResponse
    {
        [XmlElement(ElementName = "OperationRequest", Namespace = "http://webservices.amazon.com/AWSECommerceService/2011-08-01")]
        public OperationRequest OperationRequest { get; set; }
        [XmlElement(ElementName = "Items", Namespace = "http://webservices.amazon.com/AWSECommerceService/2011-08-01")]
        public Items Items { get; set; }
        [XmlAttribute(AttributeName = "xmlns")]
        public string Xmlns { get; set; }
    }
}