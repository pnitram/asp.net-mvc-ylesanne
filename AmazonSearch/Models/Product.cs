﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AmazonSearch.Models
{
    public class Product
    {
        public string title { get; set; }
        public string price { get; set; }
    }
}